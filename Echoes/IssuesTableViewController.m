//
//  IssuesTableViewController.m
//  Echoes
//
//  Created by Ed on 4/28/16.
//  Copyright © 2016 ProjectEie. All rights reserved.
//

#import "IssuesTableViewController.h"
#import "IssueTableViewCell.h"
#import "EYEEchoesClient.h"
#import "ComposeIssueTableViewController.h"

@interface IssuesTableViewController () <ComposeIssueControllerDelegate>

@property (strong, nonatomic) NSMutableArray <EYEIssue *> *issues;

@end

@implementation IssuesTableViewController

static NSString *const IssueCellIdentifier = @"Issue Cell";

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.title = @"Echoes";
  
  self.issues = [NSMutableArray arrayWithArray:[EYEEchoesClient randomIssues]];
  
  self.tableView.rowHeight = UITableViewAutomaticDimension;
  self.tableView.estimatedRowHeight = 95.f;
  
//  self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"postYourEcho"] style:UIBarButtonItemStylePlain target:self action:@selector(composeButtonAction:)];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)composeButtonAction:(id)sender {
  
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [self.issues count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  IssueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:IssueCellIdentifier forIndexPath:indexPath];
  
  EYEIssue *issue = [self.issues objectAtIndex:indexPath.row];
  
  cell.titleLabel.text = issue.title;
  cell.iconImageView.image = [UIImage imageNamed:@"echo1"];
  
  [cell setupSkills:issue.skills];
  
  
  return cell;
}


#pragma mark - ComposeIssueControllerDelegate

- (void)composeIssueController:(ComposeIssueTableViewController *)sender didSubmitIssue:(EYEIssue *)issue {
  if (issue) {
    [self.issues addObject:issue];
    [self.tableView reloadData];
  }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([segue.identifier isEqualToString:@"Compose"]) {
    ComposeIssueTableViewController *vc = (ComposeIssueTableViewController *)[[segue destinationViewController] topViewController];
    vc.delegate = self;
  }
}


@end
