//
//  EYESkill.h
//  Echoes
//
//  Created by Ed on 4/28/16.
//  Copyright © 2016 ProjectEie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EYESkill : NSObject

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *iconURL;

@end
