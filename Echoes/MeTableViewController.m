//
//  MeTableViewController.m
//  Echoes
//
//  Created by Ed on 4/28/16.
//  Copyright © 2016 ProjectEie. All rights reserved.
//

#import "MeTableViewController.h"

@interface MeTableViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;

@end

@implementation MeTableViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
//  self.profileImageView.layer.cornerRadius = self.profileImageView.bounds.size.width / 2.f;
//  self.profileImageView.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
