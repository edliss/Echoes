//
//  IssueTableViewCell.m
//  Echoes
//
//  Created by Ed on 4/28/16.
//  Copyright © 2016 ProjectEie. All rights reserved.
//

#import "IssueTableViewCell.h"
#import "EYEEchoesClient.h"

@implementation IssueTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupSkills:(NSArray *)skills {
  
  CGFloat ox = 5.f;
  CGFloat sWidth = 35.f;
  CGFloat margin  = 5.f;
  for (EYESkill *skill in skills) {
    CGRect frame = ({
      CGFloat x = ox;
      CGFloat y = 5.f;
      CGFloat width = sWidth;
      CGFloat height = sWidth;
      
      CGRectMake(x, y, width, height);
    });
    UIImageView *iconImageView = ({
      UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
      [imageView setContentMode:UIViewContentModeScaleAspectFit];
      imageView;
    });
    [iconImageView setImage:[UIImage imageNamed:skill.iconURL]];
    [self.skillsView addSubview:iconImageView];
    
    ox += sWidth + margin;
  }
}

@end
