//
//  ComposeIssueTableViewController.h
//  Echoes
//
//  Created by Ed on 4/28/16.
//  Copyright © 2016 ProjectEie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EYEEchoesClient.h"

@class ComposeIssueTableViewController;

@protocol ComposeIssueControllerDelegate <NSObject>

@optional

- (void)composeIssueController:(ComposeIssueTableViewController *)sender didSubmitIssue:(EYEIssue *)issue;

@end

@interface ComposeIssueTableViewController : UITableViewController

@property (weak, nonatomic) id <ComposeIssueControllerDelegate> delegate;

@end
