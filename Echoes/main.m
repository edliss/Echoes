//
//  main.m
//  Echoes
//
//  Created by Ed on 4/28/16.
//  Copyright © 2016 ProjectEie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
