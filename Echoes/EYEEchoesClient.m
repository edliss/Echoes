//
//  EYEEchoesClient.m
//  Echoes
//
//  Created by Ed on 4/28/16.
//  Copyright © 2016 ProjectEie. All rights reserved.
//

#import "EYEEchoesClient.h"

@implementation EYEEchoesClient

+ (NSArray *)randomIssues {
  EYEIssue *issue1 = [[EYEIssue alloc] init];
  issue1.title = @"I want to make a film";
  issue1.skills = ({
    EYESkill *skill1 = [[EYESkill alloc] init];
    skill1.title = @"Camera Operator";
    skill1.iconURL = @"cameraMan";
    
    EYESkill *skill2 = [[EYESkill alloc] init];
    skill2.title = @"Video Edditing";
    skill2.iconURL = @"videoEditor";
    
    EYESkill *skill3 = [[EYESkill alloc] init];
    skill3.title = @"Sound Engineer";
    skill3.iconURL = @"soundEngineer";
    
    @[skill1, skill2, skill3];
  });

  
  EYEIssue *issue2 = [[EYEIssue alloc] init];
  issue2.title = @"I want a drummer for a recording session";
  issue2.skills = ({
    EYESkill *skill1 = [[EYESkill alloc] init];
    skill1.title = @"Drummer";
    skill1.iconURL = @"drummer";
    
    EYESkill *skill3 = [[EYESkill alloc] init];
    skill3.title = @"Sound Engineer";
    skill3.iconURL = @"soundEngineer";
    
    @[skill1, skill3];
  });
  
  EYEIssue *issue3 = [[EYEIssue alloc] init];
  issue3.title = @"Looking for an Arabic-German interpreter";
  issue3.skills = ({
    EYESkill *skill1 = [[EYESkill alloc] init];
    skill1.title = @"Drummer";
    skill1.iconURL = @"drummer";
    
    EYESkill *skill3 = [[EYESkill alloc] init];
    skill3.title = @"Sound Engineer";
    skill3.iconURL = @"soundEngineer";
    
    @[skill1, skill3];
  });
  
  
  return @[issue1, issue2, issue3];
  
//  NSMutableArray *issues = [NSMutableArray array];
//  for (int i = 0; i < 6; i++) {
//    [issues addObject:[self randomIssue]];
//    
//  }
//  return [NSArray arrayWithArray:issues];
}

+ (EYEIssue *)randomIssue {
  EYEIssue *issue = [[EYEIssue alloc] init];
  
  issue.title = @"Any musician around here?";
  issue.skills = ({
    EYESkill *skill1 = [[EYESkill alloc] init];
    skill1.title = @"Musician";
    skill1.iconURL = @"guitart";
    
    EYESkill *skill3 = [[EYESkill alloc] init];
    skill3.title = @"Sound Engineer";
    skill3.iconURL = @"soundEngineer";
    
    @[skill1, skill3];
  });
  
  return issue;
}

+ (EYEUser *)userWithInfo:(NSDictionary *)info {
  EYEUser *user = [[EYEUser alloc] init];
  user.name = info[@"name"];
  user.picURL = info[@"pic"];
  user.skills = info[@"skills"];
  
  return user;
}

+ (EYESkill *)skillWithInfo:(NSDictionary *)info {
  EYESkill *skill = [[EYESkill alloc] init];
  
  skill.title = info[@"title"];
  skill.iconURL = info[@"icon"];
  
  return skill;
}

@end
