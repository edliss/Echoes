//
//  EYEEchoesClient.h
//  Echoes
//
//  Created by Ed on 4/28/16.
//  Copyright © 2016 ProjectEie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EYEIssue.h"
#import "EYEUser.h"
#import "EYESkill.h"

@interface EYEEchoesClient : NSObject

+ (NSArray *)randomIssues;
+ (EYEIssue *)randomIssue;

@end
