//
//  IssueTableViewCell.h
//  Echoes
//
//  Created by Ed on 4/28/16.
//  Copyright © 2016 ProjectEie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IssueTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *skillsView;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

- (void)setupSkills:(NSArray *)skills;

@end
