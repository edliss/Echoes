//
//  EYEUser.h
//  Echoes
//
//  Created by Ed on 4/28/16.
//  Copyright © 2016 ProjectEie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EYESkill.h"

@interface EYEUser : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *picURL;
@property (strong, nonatomic) NSArray <EYESkill *> *skills;

@end
