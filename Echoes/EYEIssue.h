//
//  EYEIssue.h
//  Echoes
//
//  Created by Ed on 4/28/16.
//  Copyright © 2016 ProjectEie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EYEUser.h"

@interface EYEIssue : NSObject

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSArray *skills;
@property (strong, nonatomic) EYEUser *user;


@end
