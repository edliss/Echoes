//
//  ComposeIssueTableViewController.m
//  Echoes
//
//  Created by Ed on 4/28/16.
//  Copyright © 2016 ProjectEie. All rights reserved.
//

#import "ComposeIssueTableViewController.h"

@interface ComposeIssueTableViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation ComposeIssueTableViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAction:)];
  self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveAction:)];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}
- (IBAction)cancelAction:(id)sender {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveAction:(id)sender {
  
  EYEIssue *issue = [[EYEIssue alloc] init];
  
  issue.title = self.textView.text;
  issue.skills = ({
    EYESkill *skill1 = [[EYESkill alloc] init];
    skill1.title = @"Musician";
    skill1.iconURL = @"guitar";
    
    EYESkill *skill3 = [[EYESkill alloc] init];
    skill3.title = @"Sound Engineer";
    skill3.iconURL = @"soundEngineer";
    
    @[skill1, skill3];
  });
  
  [self.delegate composeIssueController:self didSubmitIssue:issue];
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLayoutSubviews {
  [super viewDidLayoutSubviews];
  [self.textView becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  [self.textView resignFirstResponder];
}

@end
